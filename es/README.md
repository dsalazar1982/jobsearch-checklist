## Bienvenidos al Remote Jobsearh checklist

Si estás buscando trabajo remoto, y no sábes qué mejorar, por acá te dejo consejos que puedes usar de checklist.

Por favor **haz un FORK del proyecto**, y luego envía tu MR para un mejor seguimiento.

Estos consejos sobre CV los he brindado a algunos miembros de nuestra comunidad [SixFigureMentorship](https://twitter.com/search?q=from%3Aluismejiadev%20%23SixFigureMentorship&src=typed_query) en [Discord](https://discord.com/invite/ReYAw4NfF9 )

### Tabla de contenido

* [CV](#sobre-cv)
* [Linkedin](#linkedin)
* [Portafolio](#portafolio)


## Sobre tu CV
<details>
<summary>Consejos para tu CV. Marca la casilla cuando hayas aplicado el consejo.</summary>

1. [ ] Hacer consistente tu presencia en Linea: Todos tus perfiles de trabajo tienen que tener el mismo nombre y la misma foto.
1. [ ] Foto profesional: Yo te recomendaría que uses la misma foto para CV (opcional), Github y Linkedin. Elige un foto con fondo claro, y al centro tu rostro, con una sonrisa amable, y donde te veas responsable y profesional.
1. [ ] CV en inglés: Si tu objetivo es un mercado internacional, lo mejor es que tengas tu CV, linkedin y github en inglés.
1. [ ] Descripción en tu CV, Github y Linkedin: Escribe la cantidad de años de experiencia, y habla de experiencias específicas e interesantes de tus proyectos. No seas generalista porque eso no te ayudará a destacar. 
1. [ ] Personaliza tu CV: Anexa una carta de presentación a tu CV o agrega una descripción donde podés incluir cosas importante de tu experiencia que puedan ser útiles para la plaza a la que estás aplicando. Deberias personalizar, al menos un poco, el perfil para ciertos tipos de plazas: si es para desarrollador de APP moviles, destaca tu experiencia en eso; si es para web development, destaca tu experiencia en eso otro.
1. [ ] Sólo información relevante: No incluyas experiencias que no estén relacionadas al puesto al que aplicas. La lista de todos los cursos que has estudiado no aporta valor al CV, asi que sólo incluiría los relevantes a la plaza.
1. [ ] Cambia Employment History por WORK EXPERIENCE: No se trata de que conozcan la historia de tus trabajos, se trata de que conozcan tu valiosa experiencia y los principales logros que has tenido.
1. [ ] Agrega tus principales logros: Utiliza viñetas para listar tus principales logros en cada trabajo. Agrega números como porcentajes o cantidades para que puedan darse una idea del tamaño de los sistemas en los que has trabajado. Por ejemplo: cantidad de servidores, de registros en base de datos, de usuarios, de transacciones diarias. Si son aplicaciones moviles, cantidad de usuarios, cantidad de descargas.
1. [ ] Proyectos personales: Si tienes poca experiencia laboral, puedes incluir una seccion de proyectos personales, donde listes lo que has hecho, y qué conocimientos y principales logros has aquiridos. 
1. [ ] Trabajo en equipo: Si tienes experiencia en equipos remotos, grandes o multidisciplinarios, recuerda mencionarlo.

</details>




## LINKEDIN

<details>
<summary>Consejos para tu Linkedin. Marca la casilla cuando hayas aplicado el consejo.</summary>

1. [ ] Cambia tu foto según consejo anterior
1. [ ] Cambia el headline: El headline es una de las secciones más vista de tu linkedin. Utiliza un encabezado que llame la atención de los reclutadores, y que les dé ganas de hacer clic y visitar tu perfil.
1. [ ] Agrega una descripción a tu perfil de Linkein: Puede ser la misma descripción que incluyes en tu CV.
1. [ ] Agrega tus principales logros a cada una de tus experiencias en tu Lainkedin.
1. [ ] Agrega recomendaciones: Busca personas que estén satisfechas con tu trabajo, y pídeles amablemente que te escriban una recomendacion de Linkedin, en inglés de ser posible.
1. [ ] Habilidades/Skills: Busca un trabajo en Linkedin que te interese, observa las skills solicitados, y agrega las skills que vos tengas, eso hará que salgas en más búsquedas y que linkedin te sugiera trabajos con los que tus habilidades coincidan.
1. [ ] Unete a almenos 5 grupos en Linkedin: Normalmente se publican trabajos en esos grupos, que talvés no veas de otra forma.


</details>


## Portafolio

<details>
<summary>Consejos para tu Portafolio en tu Github, Gitlab o sitio web. Marca la casilla cuando hayas aplicado el consejo.</summary>

1. [ ] Utiliza la misma foto de tu Linkedin
1. [ ] Agrega tus datos de contacto
1. [ ] Agrega un REAME.md con una descripción de quién eres.
1. [ ] Destaca tus repositorios principales (pinned repo).
1. [ ] Borra los repositorios o proyectos vacios: con sólo entrar ya se ve que no hay nada.
1. [ ] No engañes con las estadisticas de Github/Gitlab: Es mejor tener poca actividad, a tener actividad ficticia con repositorios que sólo agregan cambios irrelevantes a los mismos archivos.
1. [ ] Crea un repositorio publico donde pongas en evidencia todas tus habilidades: Ese repositorio tiene que tener un README con imagenes de la app, y con pasos para instalarla, y detalles de diseños importantes que valga la pena mencionar. También tiene que tener un enlace a la app funcionando, sea en tu dominio, en heroku, en github pages, gitlab pages, aws, etc., hay varios sitios para alojar tus proyectos gratis.

# Gracias

Si te ha servido este checklist, no olvide compartir el proyecto. 
Ante cualquier duda, puedes escribirme en [twitter](https://twitter.com/luismejiadev)
